package com.blast.sspai.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import com.blast.sspai.R;

/**
 * author : hg
 * email: 530157892@qq.com
 * 15/6/15.
 */

/**
 * 发现界面
 */
public class DiscoverActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_discover);
    }
}