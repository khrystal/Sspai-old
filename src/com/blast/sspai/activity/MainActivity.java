package com.blast.sspai.activity;

import android.app.Activity;
import android.os.Bundle;
import com.blast.sspai.R;

/**
 * 主界面
 */
public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
