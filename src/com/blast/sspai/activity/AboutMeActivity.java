package com.blast.sspai.activity;

import android.app.Activity;
import android.os.Bundle;
import com.blast.sspai.R;

/**
 * Created by ibm on 2015/6/16.
 */

/**
 * 关于我们
 */
public class AboutMeActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);
    }
}