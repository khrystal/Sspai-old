package com.blast.sspai.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import com.blast.sspai.R;

/**
 * author : hg
 * email: 530157892@qq.com
 * 15/6/15.
 */

/**
 * 设置界面
 */
public class SettingsActivity extends Activity implements View.OnClickListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings);

        LinearLayout layout = (LinearLayout) findViewById(R.id.settings);
        layout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int click = view.getId();
        switch (click){
            case R.id.push_notification:
                Intent pnIntent = new Intent(this, FeedbackActivity.class);
                startActivity(pnIntent);
                break;
            case R.id.clear_cache:
                break;
            case R.id.check_version:
                break;
            case R.id.choose_type:
                break;
            case R.id.feedback:
                break;
            case R.id.about_me:
                Intent aMIntent = new Intent(this, AboutMeActivity.class);
                startActivity(aMIntent);
                break;
        }
    }

}