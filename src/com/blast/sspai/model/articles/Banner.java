package com.blast.sspai.model.articles;

/**
 * Created by dell on 2015/6/16.
 */
public class Banner {
    private String originurl;
    private String thumbUrl;
    private String thumbUrl800;
    private String thumbUrl640;
    private String thumbUrl480;
    private String thumbUrl320;
    private String thumbUrl160c;
    private String id;

    public String getOriginurl() {
        return originurl;
    }

    public void setOriginurl(String originurl) {
        this.originurl = originurl;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getThumbUrl800() {
        return thumbUrl800;
    }

    public void setThumbUrl800(String thumbUrl800) {
        this.thumbUrl800 = thumbUrl800;
    }

    public String getThumbUrl640() {
        return thumbUrl640;
    }

    public void setThumbUrl640(String thumbUrl640) {
        this.thumbUrl640 = thumbUrl640;
    }

    public String getThumbUrl480() {
        return thumbUrl480;
    }

    public void setThumbUrl480(String thumbUrl480) {
        this.thumbUrl480 = thumbUrl480;
    }

    public String getThumbUrl320() {
        return thumbUrl320;
    }

    public void setThumbUrl320(String thumbUrl320) {
        this.thumbUrl320 = thumbUrl320;
    }

    public String getThumbUrl160c() {
        return thumbUrl160c;
    }

    public void setThumbUrl160c(String thumbUrl160c) {
        this.thumbUrl160c = thumbUrl160c;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
