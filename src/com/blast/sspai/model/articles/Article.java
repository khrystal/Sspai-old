package com.blast.sspai.model.articles;

import java.util.List;

/**
 * Created by kHRYSTAL on 2015/6/16.
 */
public class Article {
    private String id;
    private String slug;
    private String status;
    private String userId;
    private String categoryId;
    private String categoryTag;
    private String gotoLink;
    private String template;
    private String title;
    private String author;
    private String keywords;
    private String description;
    private String content;
    private String views;
    private String bannerId;
    private String iconId;
    private String useImage;
    private String bannerWmp;
    private String sourceName;
    private String sourceUrl;
    private String onTop;
    private String todatOntop;
    private String disableComment;
    private String contentWmp;
    private String published;
    private String created;
    private String modified;
    private String commentCount;
    private String likeCount;
    private String favoriteCount;
    private String type;
    private String disableIcon;
    private String getWeiboCommentTime;
    private String url;
    private User user;
    private List<Tag> tags;

    private Banner banner;
    private String icon;
    private Category category;
    private String apps;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTag() {
        return categoryTag;
    }

    public void setCategoryTag(String categoryTag) {
        this.categoryTag = categoryTag;
    }

    public String getGotoLink() {
        return gotoLink;
    }

    public void setGotoLink(String gotoLink) {
        this.gotoLink = gotoLink;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getIconId() {
        return iconId;
    }

    public void setIconId(String iconId) {
        this.iconId = iconId;
    }

    public String getUseImage() {
        return useImage;
    }

    public void setUseImage(String useImage) {
        this.useImage = useImage;
    }

    public String getBannerWmp() {
        return bannerWmp;
    }

    public void setBannerWmp(String bannerWmp) {
        this.bannerWmp = bannerWmp;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getOnTop() {
        return onTop;
    }

    public void setOnTop(String onTop) {
        this.onTop = onTop;
    }

    public String getTodatOntop() {
        return todatOntop;
    }

    public void setTodatOntop(String todatOntop) {
        this.todatOntop = todatOntop;
    }

    public String getDisableComment() {
        return disableComment;
    }

    public void setDisableComment(String disableComment) {
        this.disableComment = disableComment;
    }

    public String getContentWmp() {
        return contentWmp;
    }

    public void setContentWmp(String contentWmp) {
        this.contentWmp = contentWmp;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavoriteCount(String favoriteCount) {
        this.favoriteCount = favoriteCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisableIcon() {
        return disableIcon;
    }

    public void setDisableIcon(String disableIcon) {
        this.disableIcon = disableIcon;
    }

    public String getGetWeiboCommentTime() {
        return getWeiboCommentTime;
    }

    public void setGetWeiboCommentTime(String getWeiboCommentTime) {
        this.getWeiboCommentTime = getWeiboCommentTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Banner getBanner() {
        return banner;
    }

    public void setBanner(Banner banner) {
        this.banner = banner;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getApps() {
        return apps;
    }

    public void setApps(String apps) {
        this.apps = apps;
    }
}
