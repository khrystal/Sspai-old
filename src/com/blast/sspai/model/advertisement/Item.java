package com.blast.sspai.model.advertisement;

import com.blast.sspai.model.splash.AttachMent;

/**
 * Created by kHRYSTAL on 2015/6/16.
 */
public class Item {
    private String customId;
    private String title;
    private String link;
    private String order;
    private String content;
    private AttachMent attachMent;
    private String attachmentId;

    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public AttachMent getAttachMent() {
        return attachMent;
    }

    public void setAttachMent(AttachMent attachMent) {
        this.attachMent = attachMent;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }
}
