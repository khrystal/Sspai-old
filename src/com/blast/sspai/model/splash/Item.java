package com.blast.sspai.model.splash;

/**
 * Created by kHRYSTAL on 2015/6/15.
 */
public class Item {
    private String attachmentId;
    private String title;
    private String link;
    private String content;
    private String order;
    private String type;
    private String start;
    private String end;
    private String imageUrl;
    private AttachMent attachMent;

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public AttachMent getAttachMent() {
        return attachMent;
    }

    public void setAttachMent(AttachMent attachMent) {
        this.attachMent = attachMent;
    }
}
