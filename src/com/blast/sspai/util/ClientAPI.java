package com.blast.sspai.util;

import com.blast.sspai.model.advertisement.Advert;
import com.blast.sspai.model.articles.*;
import com.blast.sspai.model.splash.AttachMent;
import com.blast.sspai.model.splash.Item;
import com.blast.sspai.model.splash.Splash;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kHRYSTAL on 2015/6/15.
 */

public class ClientAPI {
    /**
     * 首页url 是否为常量待定
     */
    public static final String BOOTSCREENURL = "http://sspai.com/ipa/ad?key=63679324&slug=sspai_client_android_boot_screen";
    public static final String ADURL="http://sspai.com/ipa/ad?key=63679324&slug=sspai_client_android_index_slide";
    public static final String ARTICLELISTURL = "http://sspai.com/ipa/article?key=63679324";
    public static final String ARTICLEURLHEAD ="http://sspai.com/ipa/article/";
    public static final String ARTICLEURLTAIL = "?key=63679324&client=android";

    /**
     * 通过首页列表的id字段获取文章的url
     * @param id
     * @return
     */
    public static String getArticleUrl(String id){
        String url = null;
        StringBuffer sb = new StringBuffer();
        if (id!=null){
            sb.append(ARTICLEURLHEAD).append(id).append(ARTICLEURLTAIL);
        }
        url = sb.toString();
        return url;
    }



    /////////////////////////////////////////////////////////////////
    //以下方法用于通过不同Json字符串获取不同的根基类【需要在异步任务或线程中处理】
    //!!因为字符串的根有可能是JsonArray 不建议使用通用方法！


    /**
     * 获取扉页的根基类Splash,内部只包含图片
     * @return
     */
    public static Splash getSplash(){
        Splash splash = null;
        String get = CommonUtiles.doGet(BOOTSCREENURL);
        if (get != null) {

            try {
                JSONObject object = new JSONArray(get).getJSONObject(0);
                JSONArray itemsArray = object.getJSONArray("items");
                List<Item> items = new ArrayList<Item>();
                for (int i = 0; i < itemsArray.length(); i++) {
                    JSONObject itemObject = itemsArray.getJSONObject(i);
                    JSONObject attachMentObject = itemObject.getJSONObject("attachment");
                    String thumbUrl800 = attachMentObject.getString("thumb_url_800");
                    AttachMent attachMent = new AttachMent();
                    attachMent.setThumbUrl800(thumbUrl800);
                    Item item = new Item();
                    item.setAttachMent(attachMent);
                    items.add(item);
                }
                splash = new Splash();
                splash.setItems(items);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return splash;
    }


    /**
     * 获取首页上方的广告
     * @return
     */
    public static Advert getAdvert(){
        Advert advert = null;
        String get = CommonUtiles.doGet(ADURL);
        if (get != null) {

            try {
                JSONObject object = new JSONArray(get).getJSONObject(0);
                JSONArray itemsArray = object.getJSONArray("items");
                List<com.blast.sspai.model.advertisement.Item> items = new ArrayList<com.blast.sspai.model.advertisement.Item>();
                for (int i = 0; i < itemsArray.length(); i++) {
                    JSONObject itemObject = itemsArray.getJSONObject(i);
                    String title = itemObject.getString("title");
                    JSONObject attachmentObject = itemObject.getJSONObject("attachment");
                    String id = attachmentObject.getString("id");
                    String thumbUrl480 = attachmentObject.getString("thumb_url_480");
                    AttachMent attachMent = new AttachMent();
                    attachMent.setThumbUrl480(thumbUrl480);
                    attachMent.setId(id);
                    com.blast.sspai.model.advertisement.Item item = new com.blast.sspai.model.advertisement.Item();
                    item.setTitle(title);
                    item.setAttachMent(attachMent);
                    items.add(item);
                }
                advert = new Advert();
                advert.setItems(items);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return advert;
    }

    /**
     * 首页下方文章列表的根基类
     * 首页下方ListView列表】【注：加载的json只取三个字段！！ 用于内容展示
     "comment_count":"x" 评论个数
     "id": "xxx" 文章id
     "title": "xxx" 标题
     * @return
     */
    public static Articles getArticles(){
        Articles articles = null;
        String get = CommonUtiles.doGet(ARTICLELISTURL);
        if (get != null) {
            try {
                JSONObject object = new JSONObject(get);
                String total = object.getString("tital");
                String perPage = object.getString("per_page");
                String currentPage = object.getString("current_page");
                String lastPage = object.getString("last_page");
                String from = object.getString("from");
                String to = object.getString("to");
                JSONArray dataArray = object.getJSONArray("data");
                List<Data> datas = new ArrayList<Data>();
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataObject = dataArray.getJSONObject(i);
                    String id = dataObject.getString("id");
                    String title = dataObject.getString("title");
                    String commentCount = dataObject.getString("commnet_count");
                    Data data = new Data();
                    data.setTitle(title);
                    data.setId(id);
                    data.setCommentCounts(commentCount);
                    datas.add(data);
                }
                articles = new Articles();
                articles.setCurrentPage(currentPage);
                articles.setDatas(datas);
                articles.setFrom(from);
                articles.setTo(to);
                articles.setTotal(total);
                articles.setPerPage(perPage);
                articles.setLastPage(lastPage);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return articles;
    }


    /**
     * 通过点击条目获取id从而获取文章的根基类
     * @param id 点击的条目
     * @return
     */
    public static Article getArticle(String id){
        Article article = null;
        String get = CommonUtiles.doGet(getArticleUrl(id));
        if (get != null) {
            try {
                JSONObject object = new JSONObject(get);
                String articleId = object.getString("id");
                String slug = object.getString("slug");
                String status = object.getString("status");
                String userId = object.getString("user_id");
                String categoryId = object.getString("category_id");
                String categoryTag = object.getString("category_tag");
                String gotoLink = object.getString("goto_link");
                String template = object.getString("template");
                String title = object.getString("title");
                String author = object.getString("author");
                String keywords = object.getString("keywords");
                String description = object.getString("description");
                String content = object.getString("content");
                String views = object.getString("views");
                String bannerId = object.getString("banner_id");
                String iconId = object.getString("icon_id");
                String useImage = object.getString("use_image");
                String bannerWmp = object.getString("banner_wmp");
                String sourceName = object.getString("source_name");

                JSONObject userObject = object.getJSONObject("user");
                String uid = userObject.getString("id");
                String nickname = userObject.getString("nickname");
                String utitle = userObject.getString("title");
                String bio = userObject.getString("bio");
                String roleId = userObject.getString("role_id");
                String avatar = userObject.getString("avatar");
                JSONObject roleObject = userObject.getJSONObject("role");
                String rid = roleObject.getString("id");
                String name = roleObject.getString("name");
                String displayName = roleObject.getString("display_name");
                String prop = roleObject.getString("prop");
                Role role = new Role();
                role.setId(rid);
                role.setName(name);
                role.setDisplayname(displayName);
                role.setProp(prop);
                User user = new User();
                user.setId(uid);
                user.setNickname(nickname);
                user.setTitle(utitle);
                user.setBio(bio);
                user.setRoleId(roleId);
                user.setAvatar(avatar);

                JSONArray tagsArray = object.getJSONArray("tags");
                List<Tag> tags = new ArrayList<Tag>();
                for (int i = 0; i < tagsArray.length(); i++) {
                    JSONObject tagObject = tagsArray.getJSONObject(i);
                    String tagId = tagObject.getString("id");
                    String tagName = tagObject.getString("name");
                    String tagDescription = tagObject.getString("description");
                    Tag tag = new Tag();
                    tag.setId(tagId);
                    tag.setName(tagName);
                    tag.setDescription(tagDescription);
                    tags.add(tag);
                }
                JSONObject bannerObject = object.getJSONObject("banner");
                String originUrl = bannerObject.getString("origin_url");
                String thumbUrl = bannerObject.getString("thumb_url");
                String thumbUrl800 = bannerObject.getString("thumb_url_800");
                String thumbUrl640 = bannerObject.getString("thumb_url_640");
                String thumbUrl480 = bannerObject.getString("thumb_url_480");
                String thumbUrl320 = bannerObject.getString("thumb_url_320");
                String thumbUrl160c = bannerObject.getString("thumb_url_160_c");
                String bId = bannerObject.getString("id");
                Banner banner = new Banner();
                banner.setId(bId);
                banner.setOriginurl(originUrl);
                banner.setThumbUrl(thumbUrl);
                banner.setThumbUrl480(thumbUrl480);
                banner.setThumbUrl320(thumbUrl320);
                banner.setThumbUrl160c(thumbUrl160c);
                banner.setThumbUrl640(thumbUrl640);
                banner.setThumbUrl800(thumbUrl800);

                String icon = object.getString("icon");
                JSONObject categoryObject = object.getJSONObject("category");
                String cId = categoryObject.getString("id");
                String cName = categoryObject.getString("name");
                String cSulg = categoryObject.getString("slug");
                String cTag = categoryObject.getString("tag");
                String indexType = categoryObject.getString("index_type");
                Category category = new Category();
                category.setId(cId);
                category.setName(cName);
                category.setSlug(cSulg);
                category.setTag(cTag);
                category.setIndexType(indexType);

                String apps = object.getString("apps");

                article = new Article();
                article.setId(id);
                article.setSlug(slug);
                article.setApps(apps);
                article.setAuthor(author);
                article.setBanner(banner);
                article.setBannerWmp(bannerWmp);
                article.setBannerId(bannerId);
                article.setCategory(category);
                article.setCategoryId(categoryId);
                article.setCategoryTag(categoryTag);
                article.setStatus(status);
                article.setUserId(userId);
                article.setGotoLink(gotoLink);
                article.setTemplate(template);
                article.setTitle(title);
                article.setKeywords(keywords);
                article.setDescription(description);
                article.setContent(content);
                article.setViews(views);
                article.setIconId(iconId);
                article.setUseImage(useImage);
                article.setSourceName(sourceName);
                //sourceUrl一下的object 解析都没写
                article.setUser(user);
                article.setTags(tags);
                article.setIcon(icon);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return article;
    }

}
